# -*- coding: utf-8 -*-

"""
Created on Sun Sep  6 12:17:06 2020

@author: Arthur Favreau
"""

import tkinter.filedialog as fd
import tkinter as tk                  # Create interface
import tkcalendar                     # Manipulate calendar in tkinter
import process
import os                             # Access files and folders
import threading                      # Allows to use interface while process is running
import time                           # Manipulate time



def open_csv():
    '''
    Browse the current folder to select the Sen4Cap csv file
    '''
    csv_val = inFile_entry.get()
    if csv_val :
        inFile_entry.delete(0, "end")
        inFile_entry.insert(0, "")
    csv_name = fd.askopenfilename(filetypes = [("csv files", "*.csv") ])
    inFile_entry.insert(0, "{}".format(csv_name))
    if inFile_entry.get():
        run_csv["text"] = "CSV : OK"
    return csv_name
        
        
def change_dates():
    """
    Recover dates in the calendar entries
    """
    if start_period_calendar.get() :
        date = start_period_calendar.get()
        lst = date.split("/")
        if len(lst[2]) == 4 :
            date_start = date
        else :
            for i, elem in enumerate(lst) :
                if len(elem) == 1 :
                    lst[i] = int("0" + str(elem))
            date_start = "{}/{}/20{}".format(lst[1], lst[0], lst[2])
    else :
        date_start = None
        
    if end_period_calendar.get() :
        date = end_period_calendar.get()
        lst = date.split("/")
        if len(lst[2]) == 4 :
            date_end = date
        else :
            for i, elem in enumerate(lst) :
                if len(elem) == 1 :
                    lst[i] = int("0" + str(elem))
            date_end = "{}/{}/20{}".format(lst[1], lst[0], lst[2])
    else :
        date_end = None
        
    return date_start, date_end

    
def get_csv_thres():
    '''
    Read the csv name and the threshold entered by the user and return them
    '''
    csv_val = inFile_entry.get()
    thres_val = thres_entry.get()
    start_val = start_period_calendar.get()
    end_val = end_period_calendar.get()
    if csv_val :
        run_csv["text"] = "CSV : OK"
    if thres_val :
        run_thres["text"] = "Threshold : OK"
    if start_val != end_val != "" :
        run_period["text"] = "Period : OK"    
    if csv_val and thres_val and start_val and end_val :
        run_button["state"] = "normal"
    return csv_val, float(thres_val)


def run_algorithm():
    '''
    Execute the CO2 flux extraction
    '''
    # if algorithm is running, PROCESS button is disabled and interface is reset
    if is_running["text"] == "Running..." :
        run_button["state"] = "disabled"
        reset_interface()
        
    # if not, then the algorithm starts to run
    else :
        is_running["text"] = "Running..."
        is_done["text"] = "                    "
        exit_button["state"] = "disabled"

        # We retrieve values of the different parameters
        csv_val, thres_val = get_csv_thres()
        date_begin, date_end = change_dates()

        # We write the parameters in the name of the result file
        result_file = '{}_CO2_flux_TIER1_{}_{}_{}.xlsx'.format(csv_val[:-4], 
                                                             thres_val, 
                                                             date_begin.replace("/", "-"),
                                                             date_end.replace("/", "-"))

        
        # Replace the current result file if it already exists
        if os.path.isfile(result_file):
            os.remove(result_file)
            
        # Disable the RUN button until the process has not finished
        run_button["state"] = "disabled"
        
        # Run the "process" algorithm
        message, ok = process.run(csv_val, thres_val, date_begin, date_end, is_running)
        
        # reset period in the interface
        reset_interface()
        
        # if error, we display the error
        if ok == False :
            is_running["text"] = "Error !"
            is_done["text"] = str(message)
            
        # if process has finished, we display the execution time
        else :
            while not os.path.isfile(result_file) :
                time.sleep(1)
            is_running["text"] = "Done !"
            is_done["text"] = str(message)


def reset_interface():
    '''
    Instructions to reset the interface when a csv has been converted
    '''
    run_csv["text"] = "CSV : "
    run_thres["text"] = "Threshold : "
    run_period["text"] = "Period : "
    start_period_calendar.delete(0, "end")
    start_period_calendar.insert(0, "")
    end_period_calendar.delete(0, "end")
    end_period_calendar.insert(0, "")
    var.set(0)
    run_button["state"] = "normal"
    exit_button["state"] = "normal"
    
    
def thread_function():
    '''
    Thread made so that the can still use the interface while a process is running
    '''
    x = threading.Thread(target=run_algorithm)
    time.sleep(1)
    x.start()
    
    
if __name__ == '__main__':
    
    form = tk.Tk()
    form.wm_title("UC1b : Computer of carbon indicator Tier 1")

    var = tk.IntVar()
    
    ''' Part 1 : Browse Sen4Cap csv'''

    stepOne = tk.LabelFrame(form, text=" 1. Enter NDVI temporal series file : ")
    stepOne.grid(row=0, columnspan=7, sticky='W', \
                 padx=5, pady=5, ipadx=5, ipady=5)
        
    inFile_label = tk.Label(stepOne, text="Select the File :")
    inFile_label.grid(row=1, column=0, sticky='E', padx=5, pady=2)

    inFile_entry = tk.Entry(stepOne)
    inFile_entry.grid(row=1, column=1, columnspan=7, sticky="WE", pady=3)

    inFile_button = tk.Button(stepOne, text="Browse ...", command=open_csv)
    inFile_button.grid(row=1, column=8, sticky='W', padx=5, pady=2)
    
    ''' Part 2 : Choose threshold '''

    stepTwo = tk.LabelFrame(form, text=" 2. Enter threshold : ")
    stepTwo.grid(row=2, columnspan=7, sticky='W', \
                 padx=5, pady=5, ipadx=5, ipady=5)
        
    thres_label = tk.Label(stepTwo, \
          text="Threshold :")
    thres_label.grid(row=3, column=0, sticky='W', padx=5, pady=2)

    thres_entry = tk.Entry(stepTwo)
    thres_entry.grid(row=3, column=1, columnspan=3, pady=2, sticky='WE')
    
    submit_thres = tk.Button(stepTwo, text = 'Submit', command = get_csv_thres) 
    submit_thres.grid(row=3, column=8, sticky='W', padx=5, pady=2)
    
    ''' Part 3 : Choose a period (Optional) '''
    
    stepThree = tk.LabelFrame(form, text=" 3. Enter indicator computation period : ")
    stepThree.grid(row=4, columnspan=7, sticky='W', \
                    padx=5, pady=5, ipadx=5, ipady=5)

    start_period_label = tk.Label(stepThree, \
          text="Period start :")
    start_period_label.grid(row=5, column=0, sticky='W', padx=5, pady=2)

    start_period_calendar = tkcalendar.DateEntry(stepThree,width=30,bg="darkblue",
                                                 fg="white",year=2020)
    start_period_calendar.grid(row=5, column=1, columnspan=3, pady=2, sticky='WE')
    
    end_period_label = tk.Label(stepThree, \
          text="Period end :")
    end_period_label.grid(row=6, column=0, sticky='W', padx=5, pady=2)

    end_period_calendar = tkcalendar.DateEntry(stepThree,width=30,bg="darkblue",
                                               fg="white",year=2020)
    end_period_calendar.grid(row=6, column=1, columnspan=3, pady=2, sticky='WE')
    
    submit_dates = tk.Button(stepThree, text = 'Submit', command = get_csv_thres) 
    submit_dates.grid(row=7, column=8, sticky='W', padx=5, pady=2)
    
    ''' Part 4 : Run the algorithm and exit the interface'''
    
    runSection = tk.LabelFrame(form, text=" 4. Process : ")
    runSection.grid(row=0, column=9, columnspan=2, rowspan=8, \
                sticky='NS', padx=5, pady=5)
        
    run_csv = tk.Label(runSection, text="CSV : ")
    run_csv.grid(row=1, column=0, sticky='W', padx=5, pady=2)
    
    run_thres = tk.Label(runSection, text="Threshold : ")
    run_thres.grid(row=2, column=0, sticky='W', padx=5, pady=2)
    
    run_period = tk.Label(runSection, text="Period : ")
    run_period.grid(row=3, column=0, sticky='W', padx=5, pady=2)
    
    run_button = tk.Button(runSection, text="PROCESS", command=thread_function)
    run_button.grid(row=10, column=0, sticky='W', padx=50, pady=2)
    
    space1 = tk.Label(runSection, text="     ")
    space1.grid(row=11, column=0, sticky='W', padx=5, pady=2)
    
    is_running = tk.Label(runSection, text="     ")
    is_running.grid(row=12, column=0, sticky='W', padx=5, pady=2)
    
    is_done = tk.Label(runSection, text="     ")
    is_done.grid(row=13, column=0, sticky='W', padx=5, pady=2)
    
    exit_button = tk.Button(runSection, text="EXIT", command=form.destroy)
    exit_button.grid(row=20, column=0, sticky='W', padx=50, pady=2)

    form.mainloop()