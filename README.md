  NIVA uc1b Tier 1 Carbon Indicator at parcel level
===================================================
Introduction
------------

This sub-project is part of the ["New IACS Vision in Action” --- NIVA]<https://www.niva4cap.eu/> project that delivers a  a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows.

This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009.

Please visit the [website]<https://www.niva4cap.eu> for further information. A complete list of the sub-projects made available under the NIVA project can be found in [gitlab](https://gitlab.com/nivaeu/)


NIVA UC1b Tier 1 carbon indicator at parcel level.
--------------------------------------------------
Installation details and complete documentation can be found in the User_guide.pdf file.

The source code relative this work is licensed under the EUPL-1.2-or-later

|EU-PL 1.2 shield|

The source code relative this work is licensed under the EUPL-1.2-or-later

|CC BY-SA 4.0 shield|

The dataset relative to this work is licensed under a `Creative Commons Attribution 4.0
International License <http://creativecommons.org/licenses/by-sa/4.0/>`__.

|CC BY-SA 4.0|

.. |EU-PL 1.2 shield| image:: https://img.shields.io/badge/license-EUPL-green.svg
   :target: https://opensource.org/licenses/EUPL-1.2
.. |CC BY-SA 4.0 shield| image:: https://img.shields.io/badge/License-CC%20BY--SA%204.0-green.svg
   :target: http://creativecommons.org/licenses/by-sa/4.0/
.. |CC BY-SA 4.0| image:: https://licensebuttons.net/l/by-sa/4.0/88x31.png
   :target: http://creativecommons.org/licenses/by-sa/4.0/
