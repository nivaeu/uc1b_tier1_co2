# -*- coding: utf-8 -*-

"""
Created on Thu Sep 10 15:23:51 2020

@author: Arthur Favreau
"""

import datetime as dt              # Date formats
from datetime import datetime      # Other import of datetime, to manipulate it easier

def period_cutter(dico, date1, date2, date_format):
    """
    Select all the acquisitions of a period.

    Parameters
    ----------
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    date1 & date2 : date
        Beginning and end of the acquisition period 
    date_format : date
        Date format in the csv input. The default value is "%d/%m/%Y".

    Returns
    -------
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    keys : list of dates
        all the keys of the dico (in chronological order)

    """
    
    keys = list(dico.keys())   # Store all dico keys in a list
    # Normally, values are already sorted in chronological order,
    # but we sort them anyway just in case
    keys.sort(key=lambda date: datetime.strptime(date, date_format))
    
    # Convert dates from DD/MM/YYYY to YYYY-MM-DD
    d1 = datetime.strptime(date1, date_format)
    d2 = datetime.strptime(date2, date_format)
    
    # Erase all the dates which are after the end of the selected period, 
    # except the aquisition just after the date, to interpolate
    if d2 < datetime.strptime(keys[-1], date_format) :
        cpt = 0
        lst_bord = []
        for i, j in enumerate(keys) :
            if datetime.strptime(j, date_format) > d2 :
                cpt += 1
                if cpt == 1 : # We keep the value which is useful to interpolate
                    lst_bord.append(keys[i-1])
                lst_bord.append(j)
                if cpt > 1 : # After, we erase every value
                    del dico[j]

        # Interpolate to find the mean value at the end of the period
        dico = interpolate_at_date_cuts(dico, keys, lst_bord[0], lst_bord[1], date2, date_format)
        
        # Sort all the dates in chronological order
        keys = list(dico.keys())
        keys.sort(key=lambda date: datetime.strptime(date, date_format))
        
        # Erase the last dico value, which is outside the period (We don't need it anymore)
        del dico[keys[-1]]
        
        # Sort all the dates in chronological order
        keys = list(dico.keys())
        keys.sort(key=lambda date: datetime.strptime(date, date_format))
             
    # Erase all the dates which are before the beginning of the selected period, 
    # except the aquisition just before the date, to interpolate
    if d1 > datetime.strptime(keys[0], date_format) :
        cpt = 0
        lst_bord = []
        for i, j in enumerate(reversed(keys)) :
            if datetime.strptime(j, date_format) < d1 :
                cpt += 1
                if cpt == 1 : # We keep the value which is useful to interpolate
                    lst_bord.append(keys[-i])
                lst_bord.append(j)
                if cpt > 1 : # After, we erase every value
                    del dico[j]
        
        # Interpolate to find the mean value at the beginning of the period
        dico = interpolate_at_date_cuts(dico, keys, lst_bord[1], lst_bord[0], date1, date_format)
    
        # Sort all the dates in chronological order
        keys = list(dico.keys())
        keys.sort(key=lambda date: datetime.strptime(date, date_format))
        
        # Erase the first dico value, which is outside the period (We don't need it anymore)
        del dico[keys[0]]
        
        # Sort all the dates in chronological order
        keys = list(dico.keys())
        keys.sort(key=lambda date: datetime.strptime(date, date_format))
    
    return dico, keys

def interpolate_at_date_cuts(dico, keys, date_before, date_after, date_to_cut, date_format):
    
    """
    Interpolate day by day at date cuts, if the two acquistions are not daily successive

    Parameters
    ----------
    dico : dictionnary
        Contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    date_before & date_after : date
        Date just before and date just after the selected starting or end date
    date_to_cut : date
        Selected starting or end date
    date_format : date
        Date format in the csv input. The default value is "%d/%m/%Y".

    Returns
    -------
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    """
        
    # We transform dates in number of days, 
    # so that we can calculate the duration between two acquisitions
    day = datetime.strptime(date_before, date_format)
    next_day = datetime.strptime(date_after, date_format)
    
    delta = (next_day - day).days  # Calculate the duration between two acquisitions
    
    # We estimate the NDVI value for each date between two acquisitions,
    # by using linear interpolation
    if delta > 1: # If the two days are not successive
        for j in range(1, delta):  # Interpolate the dates day by day
            
            # Interpolated date (First acquistion 
            # + duration between the first date and the interpolated date)
            interp_day = (day + dt.timedelta(days=j)).strftime(date_format)  
            
            if interp_day == date_to_cut :
            
                # Value of the mean NDVI for both acquisitions
                a, b = dico["{}".format(date_before)], dico["{}".format(date_after)]  
                
                # We add interpolated days in the dictionnary
                # Mean value of an interpolated date is :
                #   float(a[0]) : The mean value of the first date
                #   j/delta : Duration between the first acquisition and the interpolated date
                #   float(b[0])-float(a[0]) : Difference of means between the two acquisitions
                # We can't evaluate standard deviation for interpolated days,
                # so we consider it as a None object
                dico['{}'.format(interp_day)] = [float(a[0]) 
                                                 + (j/delta)*(float(b[0])-float(a[0])), 
                                                 None]
    
    return dico