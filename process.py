# -*- coding: utf-8 -*-


"""
Created on Wed Jul 1 09:02:07 2020
Last modification: Ludovic Arnaud 23/09/2021
@author: Arthur Favreau
"""


'''Import all the usefull libraries'''
import csv                            # Manipulate csv files
import numpy as np                    # Some math usual fonctions
import datetime as dt                 # Date formats
from datetime import datetime         # Other import of datetime, to manipulate it easier
import period_cut as pc
import time                           # Manipulate time
import pandas as pd                   # Manipulate files
import copy                           # Copy objects
import os                             # Access files and folders


def csv_nb_lines(filename):
    """
    Count the number of lines in a csv table

    Parameters
    ----------
    filename : string
        Name of the csv table, including the extension ".csv"

    Returns
    -------
    nb_lines : int
        Number of lines in the csv, including the header

    """
    with open(filename, 'r') as file:
        nb_lines = 0
        
        for line in file:
            nb_lines += 1
            
    return nb_lines


def dict_creator(csv_line):
    """
    Store all the data in a dictionnary. Each day is a key of the dictionnary, 
    and the associated value is a list of two elements :
        mean : Mean of all the NDVI values of the pixels included in the parcel
        stdev : Standard deviation of all the NDVI values of the pixels included in the parcel

    Parameters
    ----------
    csv_line : list
        line of the csv with the parcel number, the index,
        and each acquisition stocked like this : date, mean, stdev
        

    Returns
    -------
    dico : dictionnary
        Contains all the dates (keys) and the values (Mean and stdev of NDVI)

    """
    
    # Create the dictionnary which will contain all the acquisitions
    dico = {}   
    
    # if mean and stdev values are between 0 and 1
    if float(csv_line[3]) <= 1 and float(csv_line[4]) <= 1 :
        Sen4Cap = False
        
    # if mean and stdev values are between 0 and 1000, it means that the csv comes from Sen4Cap
    else :
        Sen4Cap = True
        
    # We read each element of the csv line
    for pos, elem in enumerate(csv_line) :
        
        # All lines don't contain the same number of acquisitions
        # If there's no more values in the line, we break the loop
        if elem == "" :
            break
        
        # We don't read the first two lines (Parcel number & index)
        # After, values are grouped three by three (date + mean + stdev)
        if pos%3 == 2:   
            
            # Initial format is YYYY-MM-DD, we change it to DD/MM/YYYY
            corr_date = "/".join(csv_line[pos].split('-')[::-1])


            # if mean and stdev values are between 0 and 1
            if Sen4Cap == False :
                # For each date, we associate its mean and standard deviation value
                dico['{}'.format(corr_date)] = [float(csv_line[pos+1]),
                                                float(csv_line[pos+2])]
                
            # if mean and stdev values are between 0 and 1000
            if Sen4Cap == True :
                # For each date, we associate its mean and standard deviation value
                # We divide the values by 1000 to hav values between 0 and 1
                dico['{}'.format(corr_date)] = [float(csv_line[pos+1])/1000,
                                                float(csv_line[pos+2])/1000]
            
    return dico


def min_mean_max(dico, keys):
    """
    Calculate min, mean and max of NDVI values 
    (Mean and standard deviation of pixels in the parcel)
        
    Parameters
    ----------
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    keys : list of dates
        all the keys of the dico (in chronological order)

    Returns
    -------
    mean_vals : list of floats
        min, mean and max of NDVI values of the pixels included in the parcel
    stdev_vals : list of floats
        same thing for standard deviations

    """
    
    # Create lists with all the mean (or stdev) values for a parcel
    lst_means = [dico[keys[i]][0] for i in range (0, len(keys))]
    lst_stdevs = [dico[keys[i]][1] for i in range (0, len(keys))]
    
    # Ignore None values, i.e. interpolated dates, where there's no stdev
    while None in lst_stdevs :
        del lst_stdevs[lst_stdevs.index(None)]
    
    # Calculate mins, means, and maxs
    mean_vals = [np.nanmin(lst_means), np.nanmean(lst_means), np.nanmax(lst_means)]
    stdev_vals = [np.nanmin(lst_stdevs), np.nanmean(lst_stdevs), np.nanmax(lst_stdevs)]
    
    return mean_vals, stdev_vals


def interpolate(dico, keys, threshold, date_format):
    """
    Linear interpolation day by day at threshold overruns, 
    if the two acquistions are not daily successive

    Parameters
    ----------
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    keys : list of dates
        all the keys of the dico (in chronological order)
    threshold : float
        NDVI value beyond which we consider a presence of vegetation.
        A threshold value between 0.25 and 0.45 is recommended.
    date_format : date
        Date format in the csv input. The default value is "%d/%m/%Y".

    Returns
    -------
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)

    """
    
    # We browse all the list until the penultimate element
    for i in range(len(keys) - 1):
        
        # We want to compare each acquisition to the following
        acq, next_acq = dico[keys[i]], dico[keys[i+1]]
        
        ### Threshold is substracted from acquisition and next acquisition values
        ### If the sign of these operations is different, 
        ### so the threshold has been crossed between the two dates
        if np.sign(acq[0] - threshold) != np.sign(next_acq[0] - threshold):
            
            # We transform dates in number of days, 
            # so that we can calculate the duration between two acquisitions
            day = datetime.strptime(keys[i], date_format)
            next_day = datetime.strptime(keys[i+1], date_format)
            
            # Calculate the duration between two acquisitions
            delta = (next_day - day).days  
            
            # We estimate the NDVI value for each date between two acquisitions,
            # by using linear interpolation
            if delta > 1: # If the two days are not successive
                for j in range(1, delta):  # Interpolate the dates day by day
                    
                    # Interpolated date (First acquistion 
                    # + duration between the first date and the interpolated date)
                    interp_day = (day + dt.timedelta(days=j)).strftime(date_format)  
                    
                    # Value of the mean of NDVI for both acquisitions
                    a, b = dico["{}".format(keys[i])], dico["{}".format(keys[i+1])]  
                    
                    # We add interpolated days in the dictionnary
                    # Mean value of an interpolated date is :
                    #   float(a[0]) : The mean value of the first date
                    #   j/delta : Duration between the first acquisition and the interpolated date
                    #   float(b[0])-float(a[0]) : Difference of means between the two acquisitions
                    # We can't evaluate standard deviation for interpolated days,
                    # so we consider it as a None object
                    dico['{}'.format(interp_day)] = [float(a[0]) 
                                                     + (j/delta)*(float(b[0])-float(a[0])), 
                                                     None]
    
    return dico


def threshold_overruns(dico, keys, threshold = 0.3, date_format = "%d/%m/%Y"):
    
    """
    Find threshold overruns and store them in a list
    The list with have an even number of elements, 
    because each overtaking period is the difference between two dates

    Parameters
    ----------
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    keys : list of dates
        all the keys of the dico (in chronological order)
    threshold : float
        NDVI value beyond which we consider a presence of vegetation.
        A threshold value between 0.25 and 0.45 is recommended.
    date_format : date
        Date format in the csv input. The default value is "%d/%m/%Y".

    Returns
    -------
    thres_pass : TYPE
        list of days where the threshold is crossed (begin and end of phases)

    """
    
    # List where we store the overrun dates
    thres_pass = []   
    
    # Check if the first acquisition exceeds the threshold
    # If it's true, we add the first acquisition to the list
    if dico[keys[0]][0] - threshold > 0: 
        thres_pass.append(keys[0])
    
    # We browse all the list until the penultimate element
    for i in range(0, len(keys) - 1):
        
        # We want to compare each acquisition to the following
        acq, next_acq = dico[keys[i]], dico[keys[i+1]]
        
        ### Threshold is substracted from acquisition and next acquisition values
        ### If the sign of these operations is different,
        ### so the threshold has been crossed between the days
        ### We consider the day with the maximum value (between the two days) as the threshold day
        if np.sign(acq[0] - threshold) != np.sign(next_acq[0] - threshold):
            
            if np.sign(acq[0] - threshold) < 0:
                thres_pass.append(keys[i+1])
                
            else:
                thres_pass.append(keys[i])
            
    # Check if the last acquisition exceeds the threshold
    # If it's true, we add the first acquisition to the list
    if dico[keys[-1]][0] - threshold > 0:
        thres_pass.append(keys[-1])
        
    return thres_pass


def CO2_flux(thres_pass, date_format):
    
    """
    Calculate the number of days of vegetation (days which exceed the threshold), 
    and deduce the CO2 flux with a formula

    Parameters
    ----------
    thres_pass : float
        list of days where the threshold is crossed (begin and end of phases)
    date_format : date
        Date format in the csv input. The default value is "%d/%m/%Y".

    Returns
    -------
    nb_days : int
        number of days where the threshold is crossed
    CO2 : float
        CO2 flux calculated with the number of days
    """
    
    # We initialize the number of days which exceed the threshold
    nb_days = 0   
    
    # We calculate the difference between the overrun dates two by two
    for i in range(0, len(thres_pass) - 1, 2): 
        
        # Convert dates from DD/MM/YYYY to YYYY-MM-DD
        date1 = datetime.strptime(thres_pass[i], date_format)
        date2 = datetime.strptime(thres_pass[i+1], date_format)
        
        ### Calculate the duration between two succesive threshold days, 
        ### and add it to the total number of days
        nb_days += abs((date2 - date1).days) + 1
        
    # CO2 is calculated with a formula estimated with a, empirical method
    CO2 = -0.0258*nb_days + 1.6851
    
    return nb_days, CO2


def cycle_cutter(dico, date1, date2, date_format):
    
    """
    Select all the acquisitions of a vegetation cycle.

    Parameters
    ----------
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    keys : list of dates
        all the keys of the dico (in chronological order)
    threshold : float
        NDVI value beyond which we consider a presence of vegetation.
        A threshold value between 0.25 and 0.45 is recommended.
    date_format : date
        Date format in the csv input. The default value is "%d/%m/%Y".

    Returns
    -------
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    keys : list of dates
        all the keys of the dico (in chronological order)
    """
    
    # Store all dico keys in a list
    # and sort them by chronological order
    keys = list(dico.keys())   
    keys.sort(key=lambda date: datetime.strptime(date, date_format))
    
    # Convert dates from DD/MM/YYYY to YYYY-MM-DD
    d1 = datetime.strptime(date1, date_format)
    d2 = datetime.strptime(date2, date_format)
        
    # Delete from the dico the dates which are after the end of the period
    for i, j in enumerate(keys) :
        if datetime.strptime(j, date_format) > d2 :
            del dico[j]

    # Store all dico keys in a list
    # and sort them by chronological order
    keys = list(dico.keys())
    keys.sort(key=lambda date: datetime.strptime(date, date_format))
                
    # Delete from the dico the dates which are before the beginning of the period
    for i, j in enumerate(reversed(keys)) :
        if datetime.strptime(j, date_format) < d1 :
            del dico[j]
    
    # Store all dico keys in a list
    # and sort them in chronological order
    keys = list(dico.keys())
    keys.sort(key=lambda date: datetime.strptime(date, date_format))
    
    return dico, keys


def count_cycles(dico, thres_pass, date_format):
    
    """
    Identify the vegetation cycles
    
    Parameters
    ----------
    
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    thres_pass : float
        list of days where the threshold is crossed (begin and end of phases)
    date_format : date
        Date format in the csv input. The default value is "%d/%m/%Y".

    Returns
    -------
    nb_cycles : int
        number of periods where the threshold is crossed
    lst_cycles : list
        list of cycles (One element : [Begin date, End Date, Max value of "mean"])
    """
    
    nb_cycles = 0       # Number of cycles in the period
    lst_cycles = []     # List of the cycles in the period
    
    # We calculate the difference between the overrun dates two by two
    for i in range(0, len(thres_pass) - 1, 2): 
        
        # We need to create a copy of the dict, to keep the original intact
        new_dico = copy.deepcopy(dico)
        
        # Select the acquisitions inside the cycle
        new_dico, keys = cycle_cutter(new_dico, thres_pass[i], thres_pass[i+1], date_format = "%d/%m/%Y")
        
        # Find the maximal mean value reached in the cycle
        lst_means = [new_dico[keys[i]][0] for i in range (0, len(keys))]
        maxi = round(np.nanmax(lst_means), 2)
        
        # Convert dates from DD/MM/YYYY to YYYY-MM-DD
        day = datetime.strptime(thres_pass[i], date_format)
        next_day = datetime.strptime(thres_pass[i+1], date_format)
        
        # Number of days between day and next_day
        delta = (next_day - day).days
        
        # Identify the cycles, they have a minimum duration of 30 days,
        # and they reach at least onr time a value of 0.5
        if delta >= 30 and maxi >= 0.5 :
        
            nb_cycles += 1
            lst_cycles.append([thres_pass[i], thres_pass[i+1], maxi])
        
    return nb_cycles, lst_cycles


def count_holes(dico, keys, date_format) :
    
    """
    Identify the holes (Lacks of data of more than 10 days)
    
    Parameters
    ----------
    
    dico : dictionnary
        contains all the dates (keys) and the values (Mean and standard deviation of NDVI)
    keys : list of dates
        all the keys of the dico (in chronological order)
    date_format : date
        Date format in the csv input. The default value is "%d/%m/%Y".

    Returns
    -------
    nb_holes : int
        number of periods without any valid acquisition
    lst_holes : list
        lst_holes : list of holes (One element : [Begin date, End Date, Duration])
    """
    
    nb_holes = 0
    lst_holes = []
    longest_hole = 0
    
    # We calculate the duration between the dates
    for i in range(0, len(keys) - 1) :
        
        # Convert dates from DD/MM/YYYY to YYYY-MM-DD
        date1 = datetime.strptime(keys[i], date_format)
        date2 = datetime.strptime(keys[i+1], date_format)
        
        ### Calculate the duration of the hole
        delta = abs((date2 - date1).days)
        
        # The longest hole is actualized when we find a longer duration
        if delta > longest_hole :
            longest_hole = delta
        
        # If the duration is more than 10 days (Acquisitions are all the 5 days more of less),
        # so the hole is an important lack of data, and we add it to the list
        if delta > 10 :
            nb_holes += 1
            lst_holes.append([keys[i], keys[i+1], delta])
        
    return nb_holes, lst_holes, longest_hole
    

def run(in_file, threshold, date1, date2, is_running):
    
    """
    Create new csv with all the useful data :
        KOD_PB : Original parcel id (Already present in the original csv)
        SUFFIX : NDVI (Already present in the original csv)
        CO2_FLUX : Calculated CO2 flux
        NB_DVEG : Calculated number of days over the threshold
        NB_ACQS : Calculated number of acquisitions during the period
        FIRST_ACQ, LAST_ACQ : Dates of the first and last acquisitions
        NB_CYCLES, LST_CYCLES : Number and detail of the cycles
        NB_HOLES, LNGST_HOLE, LST_HOLES : Number and detail of the holes
        MIN_VAL, AVG_VAL, MAX_VAL : Min, mean and max of the NDVI values for the parcel
        MIN_STD, AVG_STD, MAX_STD : Same for the standard deviation
    
    Parameters
    ----------
    
    in_file : string
        Name of the input csv table (Has to be in the same path as the script)
    threshold : float
        NDVI value beyond which we consider a presence of vegetation.
        A threshold value between 0.25 and 0.45 is recommended.
    date1 & date2 : date
        Beginning and end of the acquisition period 
    is_running : string
        text displayed in real time in the interface during calculation
        

    Returns
    -------
    message : string
        Excution time whether the calculation worked well, and error if not
    ok : bool
        Indicates if an error occured during the process
    """

    # if the script stops because of an error, an error message appears in the interface
    # else, the csv containing the CO2 flux and all the useful data is created
    try :
        
        start_time = time.time()  # Beginning of execution -> Calculate the execution time
        
        ok = False
        
        ''' Global values '''
        corr_file = '{}_one_separator.csv'.format(in_file[:-4])   # csv file with only one separator (;)
        temp_file = '{}_temp.csv'.format(in_file[:-4])   # Output csv file will all the useful data
        date_format = "%d/%m/%Y"   # day/month/year, with the following format : DD/MM/YYYY in numbers
        
        # if the selected file is not a csv table, error
        if in_file[-3:] != 'csv' :
            message = "Error : Your table is not a csv"
        
        # if there is no threshold, error
        if threshold == None :
            message = "Error : You have to select a threshold !"
            print(message)
            raise
        
        # if threshold is not a number, error
        if type(threshold) is not float :
            message = "Error : Threshold must be a number !"
            print(message)
            raise
            
        # if threshold is not between 0 and 1, error
        if threshold < 0 or threshold > 1 :
            message = "Error : Threshold must be between 0 and 1 !"
            print(message)
            raise
            
        # Pertinent threshold value is between 0.25 and 0.45
        # We display a warning message is the selected threshold is outside
        if threshold < 0.25 or threshold > 0.45 :
            message = "Warning : Choose a threshold around 0.3 (between 0.25 and 0.45) is more pertinent"
            print(message)
        
        # if no dates were selected
        if date1 == None or date2 == None :
            message = "Error : You have to select the period !"
            print(message)
            raise

        # Convert dates to compare them easily
        d1 = datetime.strptime(date1, date_format)
        d2 = datetime.strptime(date2, date_format)
        
        # if the starting date is after the end date, error
        if d1 > d2 :
            message = "Error : End of the period is before beginning of the period !"
            print(message)
            raise

        # Calculates the number of lines in the csv, and display it
        nb_lines = csv_nb_lines(in_file)
        print("--- Number of lines : {} ---".format(nb_lines))
        
        # There are two separators in the original Sen4Cap csv file
        # We just need one, so we keep only the ; separator
        with open(in_file, mode = "r") as f: # Read the original csv
            csv_data = f.read().replace("|", ";") # Replace all / by ; and store this in csv_data
        
        # Save the csv with only one separator
        with open(corr_file, mode = "w") as corr_f: # Create a new csv
            corr_f.write(csv_data) # Write the csv_data in the new csv
        
        # Intialize the progression of the calcultation
        print("--- Process started ---")
        former_progress = 0
        print("Process : 0% done")
        
        
        ### Now we can create the final csv
        with open(corr_file, mode='r') as in_f:   # Open the corrected csv in reading
            with open(temp_file, mode='w', newline = '') as out_f:   # Create a new csv and allows to write on it
                
                line_reader = csv.reader(in_f, delimiter=',')   # Allows to read the corrected csv
                line_writer = csv.writer(out_f, delimiter=',')  # Allows to write in the new csv
                
                # The first line of the csv is for column names
                header = True   
                
                # We read the csv line by line
                line_number = 0
                for row in line_reader:
                    
                    # Put all values of a line in a list
                    readline = (', '.join(row)).split(';')
                    
                    # if the csv doesn't have the good separator, error
                    if len(readline) == 1:
                        message = "Error : Your csv separator is not a semicolon !"
                        print(message)
                        raise
                        
                    # Add the first two columns of the csv in the new one (They don't change)
                    writeline = readline[0:2]   
                    
                    # Fill the first line of the csv with columns names
                    if header == True:   
                        
                        writeline.append("CO2_FLUX")
                        writeline.append("NB_DVEG")
                        writeline.append("NB_ACQS")
                        writeline.append("FIRST_ACQ")
                        writeline.append("LAST_ACQ")
                        writeline.append("NB_CYCLES")
                        writeline.append("LST_CYCLES")
                        writeline.append("NB_HOLES")
                        writeline.append("LNGST_HOLE")
                        writeline.append("LST_HOLES")
                        writeline.append("MIN_NDVI")
                        writeline.append("AVG_NDVI")
                        writeline.append("MAX_NDVI")
                        writeline.append("MIN_STD")
                        writeline.append("AVG_STD")
                        writeline.append("MAX_STD")
                        
                        header = False  
                        
                    # Now that the header is done, we can fill the other lines
                    else :
                        
                        # Create the dictionnary 
                        dico = dict_creator(readline)     
                            
                        # Select all the acquisitions in the period
                        dico, keys = pc.period_cutter(dico, date1, date2, date_format)
                        
                        # Find the first and the last acquisition in the period
                        first_acq, last_acq = keys[0], keys[-1]
                        
                        # Specify out_file 
                        out_file = '{}_CO2_flux_TIER1_{}_{}_{}.xlsx'.format(in_file[:-4], threshold, 
                                                                             date1.replace("/", "-"), date2.replace("/", "-"))   # Same csv with correct column types
    
                        # Count the holes
                        nb_holes, lst_holes, longest_hole = count_holes(dico, keys, date_format) # Indicate the holes
                        
                        # Number of acquisitions during the period
                        nb_acqs = len(keys)   
                        
                        # mins, means and maxs of NDVI during the period
                        mean_vals, stdev_vals = min_mean_max(dico, keys) 
                        
                        # Interpolate at threshold crossings
                        dico = interpolate(dico, keys, threshold, date_format)  
                        
                        # New list, with interpolated values added
                        # We need to sort the list, because the interpolated values were added after
                        # the acquisition values, and it could false the results        
                        keys = list(dico.keys())
                        keys.sort(key=lambda date: datetime.strptime(date, date_format))
                        
                        # Find threshold overruns
                        thres_pass = threshold_overruns(dico, keys, threshold, date_format)   # Find the threshold overrun periods
                        
                        # Calculate the CO2 flux
                        nb_days, CO2 = CO2_flux(thres_pass, date_format)  # Calculate the number of days over the threshold and the CO2 flux
                    
                        # Count the cycles
                        nb_cycles, lst_cycles = count_cycles(dico, thres_pass, date_format) # Indicate the cycles
                    
                        # We round the CO2 flux value to match the precision
                        # of the variables in the CO2 flux formula
                        writeline.append(float(round(CO2, 2)))  
                        writeline.append(int(nb_days))
                        writeline.append(int(nb_acqs))
                        writeline.append(first_acq)
                        writeline.append(last_acq)
                        writeline.append(nb_cycles)
                        writeline.append(lst_cycles)
                        writeline.append(nb_holes)
                        writeline.append(longest_hole)
                        writeline.append(lst_holes)
                        
                        # We round mins, means and maxs because we don't need more precision
                        writeline.append(float(round(mean_vals[0], 2)))
                        writeline.append(float(round(mean_vals[1], 2)))
                        writeline.append(float(round(mean_vals[2], 2)))
                        writeline.append(float(round(stdev_vals[0], 2)))
                        writeline.append(float(round(stdev_vals[1], 2)))
                        writeline.append(float(round(stdev_vals[2], 2)))
    
                    # increment the number of lines and actualize the progress
                    line_number += 1
                    progress = int(100*(line_number/nb_lines))
                    if progress > former_progress :
                        print("Process : {}% done".format(progress))
                        former_progress = progress
                        is_running['text'] = "Process : {}% done".format(progress)
                    
                    # Write the results in the output table
                    line_writer.writerow(writeline)  # All the content is writen in the new csv
        
        # Convert csv to excel table, in order to have columns types
        # Output table is an xlsx file with two sheets :
        #   Sheet 1 : Metadata (Name of csv, threshold and period)
        #   Sheet 2 : Results
        df1 = pd.DataFrame([['File', in_file], ['Threshold', threshold], 
                            ['Period start', date1], ['Period end', date2]],
                           index=['File', 'Threshold', 'Period start', 'Period end'],
                           columns=['Name', 'Value'])
        df2 = pd.read_csv(temp_file).infer_objects().round(decimals=2)
    
        with pd.ExcelWriter(out_file) as writer :
            df1.to_excel(writer , index = False, sheet_name = 'Metadata')
            df2.to_excel(writer , index = False, sheet_name = 'Results')
    
        os.remove(temp_file)
        
        print("--- Process finished ---")
        
        # We calculate the execution time of the algorithm
        message = "Execution time : %s seconds" % (int(time.time() - start_time) + 1)
        
        print("--- {} ---".format(message))
        
        # No error, process finished
        ok = True
        
        return message, ok
    
    except Exception as e :
        message = str(e)
        print(message)
        return message, ok
    
if __name__ == "__main__" :  # Run the algorithm
    run()
